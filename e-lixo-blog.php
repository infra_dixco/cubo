<?php include 'header.html'; ?>


    <!--================Hero Banner Area Start =================-->
    <section class="hero-banner d-flex align-items-center">
        <div class="container text-center">
            <h2>O que é e-Lixo?</h2>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">e-lixo</li>
                </ol>
            </nav>
        </div>
    </section>
    <!--================Hero Banner Area End =================-->

    <!-- Start Sample Area -->
    <section class="sample-text-area">
        <div class="container">
            <h3 class="text-heading title_color" style="text-align: center;">E-LIXO NÃO SE JOGA NO LIXO COMUM!</h3>
            <br>
            <div class="row">
                <div class="col-md-6" style="padding-top: 35px;">
                    <p class="sample-text">
                        Também é conhecido como e-lixo ou REEE (Resíduos de Equipamentos Eletrônicos), o lixo eletrônico
                        é todo
                        o resíduo produzido pelo descarte de equipamentos eletrônicos.
                        <br>
                        <br>
                        Se descartados de forma incorreta, podem contaminar os terrenos e as águas, causando sérios
                        riscos ao meio ambiente e à nossa saúde.
                        <br>
                        <br>
                        Esses equipamentos contêm uma série de componentes químicos e substâncias altamente poluentes.

                    </p>
                    <br>
                </div>

                <div class="col-md-6">
                    <blockquote class="generic-blockquote">
                        <img src="img/02-TOXINAS_v2.png" alt="">
                    </blockquote>
                </div>
            </div>

        </div>
    </section>
    <!-- End Sample Area -->


    <!-- Start Sample Area -->
    <section class="sample-text-area">
        <div class="container">
            <!--            <h3 class="text-heading title_color" style="text-align: center;">E-LIXO NÃO SE JOGA NO LIXO COMUM!</h3>-->
            <br>
            <div class="row">
                <div class="col-md-8">
                    <img src="img/03-desafio2.png" alt="">
                </div>

                <div class="col-md-4" style="padding-top: 50px;">
                    <blockquote class="generic-blockquote">
                        Apenas 3% do lixo eletrônico produzido têm o encaminhamento correto. Isso significa que a quase
                        totalidade do e-lixo produzido é descartado sem nenhum tratamento, representando riscos à saúde
                        na natureza.

                    </blockquote>
                    <br>
                </div>


            </div>

        </div>
    </section>
    <!-- End Sample Area -->


    <!-- Start Sample Area -->
    <section class="sample-text-area">
        <div class="container">
            <!--            <h3 class="text-heading title_color" style="text-align: center;">E-LIXO NÃO SE JOGA NO LIXO COMUM!</h3>-->
            <br>
            <div class="row">

                <div class="col-md-4" style="padding-top: 50px;">
                    <p>
                        Você pode ajudar a reduzir este impacto causado pelo descarte incorreto e a <strong
                                style="color: #277700;">Cubo Recicla</strong>
                        está pronta a ser seu braço direito nesta empreitada.

                        <br>
                        <br>

                        No Brasil, a destinação correta do e-lixo está prevista na Política Nacional de Resíduos Sólidos
                        (Lei 12.305/2010) e é regulamentada pelo
                        <a href="https://www.in.gov.br/en/web/dou/-/decreto-n-10.240-de-12-de-fevereiro-de-2020-243058096">Decreto
                            Federal 10.240/2020</a>
                    </p>

                    <br>
                    <br>
                    <br>
                    <small>Fonte:
                        https://agenciabrasil.ebc.com.br/geral/noticia/2021-10/brasil-e-o-quinto-maior-produtor-de-lixo-eletronico</small>
                </div>


                <div class="col-md-8">
                    <blockquote class="generic-blockquote">
                        <img src="img/cenario_v4.png" alt="">
                    </blockquote>
                </div>


            </div>


        </div>
    </section>
    <!-- End Sample Area -->


<?php include 'footer.html'; ?>