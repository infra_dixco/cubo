<?php include 'header.html'; ?>

<!--================Hero Banner Area Start =================-->
<section class="hero-banner d-flex align-items-center">
    <div class="container text-center">
        <h2>Quem somos?</h2>
        <nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Sobre nós</li>
            </ol>
        </nav>
    </div>
</section>
<!--================Hero Banner Area End =================-->


<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color"><i class="ti-check"></i>&nbsp; Sobre a Cubo Recicla</h3>
        <p class="sample-text">
            A Cubo Recicla atua a mais de 4 anos, no gerenciamento de resíduos eletroeletrônicos de informática, e em
            geral, sucatas diversas. Recolhemos estes materiais e por meio de processos de triagem, separamos
            componentes como: alumínio, ferro, cobre e plástico. Desta forma, seus materiais eletrônicos entram na
            cadeia da logística reversa, que é muito importante para nossa natureza.
        </p>
        <br>
        <p>
            Com sede em Guarulhos – São Paulo, possuímos toda a documentação necessária, para atender e assegurar a
            destinação correta para a sua empresa e de seus colaboradores, onde podemos atuar juntos com um ponto de
            coleta e conscientização da população, na importância de um descarte correto. Coletamos e recebemos uma
            infinidade de objetos que através de nossa Logística Reversa, podem ser reaproveitados no comércio ou
            encaminhados à indústria para reciclagem.
        </p>
        <p>
            Um Grupo Humanizado, com uma nova visão em relação à sustentabilidade e o impacto de nossas ações.
        </p>
    </div>
</section>
<!-- End Sample Area -->


<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color"><i class="ti-check"></i>&nbsp; Missão, Visão e Valores</h3>
        <div class="col-md-8">
            <p>
                <strong style="color: #277700; font-size: 18px;"><em>Nossa Missão</em></strong>
                <br>
                Causar menos impactos no meio ambiente, contribuindo com qualidade e responsabilidade social para o
                desenvolvimento sustentável.
            </p>
            <br>
            <p>
                <strong style="color: #277700; font-size: 18px;"><em>Nossa Visão</em></strong>
                <br>
                Lixo eletrônico bem tratado não é lixo, mas sim um material de logística reversa. Onde podemos assim
                contribuir com a preservação do meio ambiente, pois essa logística reversa, permitirá que estes
                materiais
                voltem a sua cadeia de produção como matéria-prima.
            </p>
            <br>

            <strong style="color: #277700; font-size: 18px;"><em>Nossos Valores</em></strong>
            <br>
            <ul class="ordered-list">
                <li><span>Respeito ao Meio Ambiente</span></li>
                <li><span>Preservação da Saúde e Segurança dos nossos funcionários</span></li>
                <li><span>Ética e transparência no relacionamento com todas as partes interessadas</span></li>
                <li><span>Compromisso com a verdade</span></li>
                <li><span>Melhoria contínua em todos os processos</span></li>
            </ul>
        </div>

    </div>
</section>
<!-- End Sample Area -->

<br>
<br>


<?php include 'footer.html'; ?>
