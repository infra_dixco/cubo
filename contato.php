<?php include 'header.html'; ?>

<!--================Hero Banner Area Start =================-->
<section class="hero-banner d-flex align-items-center">
    <div class="container text-center">
        <h2>Contato</h2>
        <nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contato</li>
            </ol>
        </nav>
    </div>
</section>
<!--================Hero Banner Area End =================-->


<!-- ================ contact section start ================= -->
<section class="contact-section area-padding">
    <div class="container">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.309171926479!2d-46.556191149923414!3d-23.485370964599444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5f81c43c9e05%3A0xc000d1af8bd691cb!2sR.%20Sd.%20Tom%C3%A1s%20Ant%C3%B4nio%20Machado%2C%2036%20-%20Vila%20Esplanada%2C%20Guarulhos%20-%20SP%2C%2007041-270!5e0!3m2!1spt-BR!2sbr!4v1623907646966!5m2!1spt-BR!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>


        <br>
        <br>

        <div class="row">
            <div class="col-12">
                <h2 class="contact-title">Escreva para nós</h2>
            </div>
            <div class="col-lg-8">
                <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" name="name" id="name" type="text" placeholder="Seu Nome">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" name="email" id="email" type="email" placeholder="Seu E-mail">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <input class="form-control" name="subject" id="subject" type="text" placeholder="Assunto">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" placeholder="Mensagem"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="form-group mt-3">
                        <button type="submit" class="button button-contactForm">Enviar</button>
                    </div>
                </form>


            </div>

            <div class="col-lg-4">
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-home"></i></span>
                    <div class="media-body">
                        <h3>Rua Soldado Tomás Antônio Machado, 36 </h3>
                        <p>Vila Esplanada, Guarulhos - SP <br> CEP: 07041-270</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                    <div class="media-body">
                        <h3><a href="tel:01145744302">(11) 4574-4302</a></h3>
                        <h3><a href="tel:011933121990">(11) 93312-1990</a></h3>
                        <p>Segunda à Sexta das 8:00 as 18:00</p>
                    </div>
                </div>
                <div class="media contact-info">
                    <span class="contact-info__icon"><i class="ti-email"></i></span>
                    <div class="media-body">
                        <h3><a href="mailto:suporte@dixco.com.br">contato@cuborecicla.com.br</a></h3>
                        <p>E-mail de contato</p>
                    </div>
                </div>
<!--                <div class="media contact-info">-->
<!--                    <div class="media-body">-->
<!--                        <a href="#">-->
<!--                            <span class="contact-info__icon"><i class="ti-linkedin"></i></span>-->
<!--                        </a>-->
<!--                        <a href="#">-->
<!--                            <span class="contact-info__icon"><i class="ti-instagram"></i></span>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</section>
<!-- ================ contact section end ================= -->

<?php include 'footer.html'; ?>
