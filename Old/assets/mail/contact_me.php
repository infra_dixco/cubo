<?php
//Check for empty fields
if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone']) || empty($_POST['message']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  http_response_code(500);
  exit();
}

$email_remetente="rafaelreis90@hotmail.com"; //Nosso email

// Variaveis recebidas do form
$nome = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$assunto = strip_tags(htmlspecialchars($_POST['phone']));
$msg = strip_tags(htmlspecialchars($_POST['message']));

// Create the email and send the message
$to = $email_remetente; 
$subject = "Contato do Site:  $assunto\n";
$body = "<center><h1><b>Você recebeu uma nova mensagem do Site.</b></h1></center><br /><br /><b>Nome:</b> $nome<br /><b>Email:</b> $email<br /
><b>Mensagem:</b><br />$msg\n";
$headers = "MIME-Version: 1.1\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$headers .= "From: AppLink Bio <$email_remetente>\n";
$headers .= "Return-Path: AppLink Bio <$email_remetente>\n"; 
$headers .= "Reply-To: $email\n";	
$headers .= "X-Priority: 1\n";

$quebra_linha ="\n";

if(!mail($to, $subject, $body, $headers ,"-r".$email_remetente)){ // Se for Postfix
    $headers .= "Return-Path: " . $email_remetente . $quebra_linha; // Se "não for Postfix"
    mail($to, $subject, $body, $headers );
}

?>
