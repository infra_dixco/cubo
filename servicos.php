<?php include 'header.html'; ?>

<!--================Hero Banner Area Start =================-->
<section class="hero-banner d-flex align-items-center">
    <div class="container text-center">
        <h2>Nossos Serviços</h2>
        <nav aria-label="breadcrumb" class="banner-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Serviços</li>
            </ol>
        </nav>
    </div>
</section>
<!--================Hero Banner Area End =================-->

<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color" style="text-align: center;">Coleta - Pessoa Física</h3>
        <br>
        <div class="row">
            <div class="col-md-8" style="padding-top: 35px;">
                <p class="sample-text">
                    Você é pessoa física e tem pequenas quantidades de lixos eletrônicos, como por exemplo: monitor,
                    impressora, computadores.
                    <br>
                    Entre em contato conosco, e caso sua região seja atendida, adequaremos
                    nosso itinerário e faremos a coleta dos lixos eletrônicos em sua residência, tudo para seu conforto.

                </p>
                <!--                <a href=""></a>-->
                <br>
            </div>

            <!--            <div class="col-md-6">-->
            <!--                <blockquote class="generic-blockquote">-->
            <!--                    <img src="img/02-TOXINAS_v2.png" alt="">-->
            <!--                </blockquote>-->
            <!--            </div>-->
        </div>

    </div>
</section>
<!-- End Sample Area -->


<!-- Start Sample Area -->
<section class="sample-text-area">
    <div class="container">
        <h3 class="text-heading title_color" style="text-align: center;">Coleta – Pessoa jurídica</h3>
        <br>
        <div class="row">
            <div class="col-md-8" style="padding-top: 35px;">
                <p class="sample-text">
                    Para você que se enquadra no perfil de pessoa jurídica (escolas e empresas) e quer fazer uma
                    parceria com a Cubo Recicla, estaremos juntos nessa parceria em prol do meio ambiente.

                    <br>
                    Nós disponibilizamos coletores fixos e materiais explicativos para deixar no estabelecimento e
                    fazemos a coleta sempre que solicitado.
                    <br>
                    <br>

                    Quer levar essa novidade para sua empresa? Esperamos o seu contato.


                </p>
                <!--                <a href=""></a>-->
                <br>
            </div>

            <!--            <div class="col-md-6">-->
            <!--                <blockquote class="generic-blockquote">-->
            <!--                    <img src="img/02-TOXINAS_v2.png" alt="">-->
            <!--                </blockquote>-->
            <!--            </div>-->
        </div>

    </div>
</section>
<!-- End Sample Area -->
<div class="container">
    <div class="offset-md-4 col-md-12" style="padding-left: 50px;">
            <span >
                <a class="banner_btn" href="#servicos">Entre em contato conosco!<i
                            class="ti-arrow-down"></i></a>

            </span>
    </div>
</div>
<br>
<br>
<br>


<!--<section class="sample-text-area">-->
<!--    -->
<!--</section>-->

<?php include 'partnership.php'; ?>
<?php include 'footer.html'; ?>


