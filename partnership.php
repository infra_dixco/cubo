<!--================Feature  Area =================-->
<section class="feature-area area-padding bg_one">
    <div class="row">

        <div class="col-md-12">
            <div class="area-heading light text-center">
                <h4>Seja nosso parceiro!</h4>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row ">
            <p class="area-heading" style="color:#FFFFFF;">
                <strong style="color:#FFFFFF; font-size: 18px;"><em>Empresas:</em></strong> &nbsp; Que tal ter sua empresa como nossa parceira? Sabe aqueles lixos eletrônicos que você não
                pode descartar em qualquer ambiente? Nós coletamos e reciclamos para você! Entre em contato e saiba
                mais!




            </p>

            <p class="area-heading" style="color:#FFFFFF;">
                <strong style="color: #FFFFFF; font-size: 18px;"><em>Escolas:</em></strong> &nbsp; Chame a diretora! Você aluna(o), professor ou funcionário de escola, entre em contato
                conosco e veja a possibilidade de obter os coletores conscientes em sua escola. Seja o exemplo em
                sua região.
            </p>
        </div>
    </div>


    <div class="container">
        <div class="offset-md-4 col-md-12" style="padding-left: 50px;">
            <span >
                <a class="banner_btn" href="#servicos">Entre em contato conosco!<i
                            class="ti-arrow-down"></i></a>

            </span>
        </div>
    </div>
</section>

<!--================Feature Area End =================-->
