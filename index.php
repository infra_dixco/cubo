<?php include 'header.html'; ?>

    <!--================Home Banner Area =================-->
    <section class="home_banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="overlay"></div>
            <div class="container">

                <div class="banner_content">
                    <h3>Somos a Cubo Recicla</h3>
                    <p>Somos uma empresa multidisciplinar, integrada por especialistas e profissionais da área
                        ambiental. Nossos serviços proporcionando para a sua empresa as melhores soluções ambientais,
                        colaborando para que tenham mais sustentabilidade e lucratividade, oferendo e prestando serviços
                        ambientais de qualidade aos empreendedores e a sociedade, com soluções, ideias praticas,
                        conhecimentos sólidos e técnicos.
                    </p>
                    <a class="banner_btn" href="about-us.php">Saiba mais sobre nós<i class="ti-arrow-right"></i></a>
                </div>

            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->


    <!--================Service  Area =================-->
    <!--<section class="service-area area-padding" id="servicos">-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-6 col-lg-4">-->
    <!--                <div class="single-service">-->
    <!--                    <div class="service-icon">-->
    <!--                        <i class="ti-pencil-alt"></i>-->
    <!--                    </div>-->
    <!--                    <div class="service-content">-->
    <!--                        <h5>Licenciamento Ambiental</h5>-->
    <!--                        <p></p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!---->
    <!--            <div class="col-md-6 col-lg-4">-->
    <!--                <div class="single-service">-->
    <!--                    <div class="service-icon">-->
    <!--                        <i class="ti-image"></i>-->
    <!--                    </div>-->
    <!--                    <div class="service-content">-->
    <!--                        <h5>Certificado de movimentação de resíduos de interesse ambiental (CADRI)</h5>-->
    <!--                        <p></p>-->
    <!--                        <a href="produtos.html">Saiba Mais</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!---->
    <!--            <div class="col-md-6 col-lg-4">-->
    <!--                <div class="single-service">-->
    <!--                    <div class="service-icon">-->
    <!--                        <i class="ti-headphone-alt"></i>-->
    <!--                    </div>-->
    <!--                    <div class="service-content">-->
    <!--                        <h5>Gerenciamento de resíduos (PGRS, MTR, DMR)</h5>-->
    <!--                        <p></p>-->
    <!--                        <a href="produtos.html">Saiba Mais</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!---->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!--================Service Area end =================-->


    <br>
    <br>
    <br>
    <br>

    <!--================About  Area =================-->
    <section class="about-area area-padding-bottom " id="sobre" style="padding-top: 50px">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-md-6">
                    <div class="area-heading">
                        <h3>Conheça os principais lixos que nos coletamos</h3>
                        <h6>A <strong style="color: #277700;"><em>Cubo Recicla</em></strong>, age no segmento de
                            reciclagem de e-lixos, que realiza a coleta de vários tipos de eletrônicos. Entre eles:</h6>

                        <div class="row">
                            <div class="col-md-6">
                                <ul class="unordered-list">
                                    <li>Computadores</li>
                                    <li>Notebooks</li>
                                    <li>Impressoras</li>
                                    <li>Monitores</li>
                                    <li>Televisores</li>

                                </ul>
                            </div>

                            <div class="col-md-6">
                                <ul class="unordered-list">
                                    <li>Pilhas</li>
                                    <li>Carregadores</li>
                                    <li>Bateria</li>
                                    <li>Entre outros...</li>
                                </ul>
                            </div>
                        </div>


                        <!--                        <p>O fato é que por gerar curiosidade dos pedestres, muitos chegam próximos e nos perguntam o que estamos fazendo e adivinhem só? Causam alteração no resultado da medição!! Mas tudo bem, com os softwares de hoje conseguimos consertar estes detalhes </p>-->
                    </div>

                    <a class="banner_btn" href="#servicos">Veja abaixo como funciona a coleta<i class="ti-arrow-down"></i></a>

                    <!-- <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="single-about">
                                <div class="about-icon">
                                    <i class="ti-thought"></i>
                                </div>
                                <div class="single-about-content">
                                    <h5></h5>
                                    <p></p> </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="single-about">
                                <div class="about-icon">
                                    <i class="ti-truck"></i>
                                </div>
                                <div class="single-about-content">
                                    <h5></h5>
                                    <p> </p>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>

                <div class="col-md-6">
                    <h1>imagem coleta</h1>
                </div>
            </div>
        </div>
    </section>
    <!--================About Area End =================-->
    <br>
    <br>
    <!--================Feature  Area =================-->
    <section class="feature-area area-padding bg_one">
        <div class="row">

            <div class="col-md-5">
                <div class="image-box">
                    <img src="img/01-E-lixo_v2.png" alt="">
                </div>
            </div>

            <div class="offset-md-1 col-md-6">
                <div class="area-heading light">
                    <h4>O que é um e-lixo?<br/>
                    </h4>

                    <p>Podemos definir como lixo eletrônico ou e-lixo tudo o que é proveniente de equipamentos eletroeletrônicos, incluindo celulares, computadores, impressoras, entre outros.</p>
                    <p>A tecnologia está em constante transformação. Novos equipamentos surgem em ritmo acelerado; mais rápidos, mais práticos e mais modernos. A medida que uma tecnologia é substituída, muitos objetos tornam-se obsoletos e são descartados.</p>
                    <p>
                        É comum, hoje em dia, trocarmos de celular a cada ano, comprarmos um computador mais moderno, uma TV maior para a sala, eletrodomésticos que facilitam a vida, e vários outros aparelhos eletrônicos que estão presentes no dia a dia.
                        <br>
                        Quando danificados ou em sem uso, na maioria dos casos, esses produtos são descartados no lixo comum. Porém eles podem ser reciclados, ou seja, pode ser transformado em outras matérias-primas em vez de irem parar em aterros sanitários.
                    </p>
                </div>

            </div>
        </div>
    </section>
    <!--================Feature Area End =================-->


    <!--================About  Area =================-->
    <section class="statics-area area-padding">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="image-box">
                        <img src="img/Goods.png" alt="" style="width: auto; padding-top: 100px;">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="area-heading">
                        <h4>Como funciona nossa coleta ?</h4>
                        <blockquote class="generic-blockquote">
                            <strong style="font-size: 18px"><em>Primeiro contato:</em></strong> &nbsp; Possui os lixos eletrônicos separados? Então basta entrar em contato
                            conosco para agendar o dia e o horário da coleta. Para você que se encaixa no segmento de
                            parcerias (empresas e escolas), fornecemos coletores e materiais explicativos para deixar no
                            local cadastrado; solicite no seu no primeiro contato.
                            <br>
                            <br>
                            <strong style="font-size: 18px"><em>A Coleta:</em></strong> &nbsp; Após o agendamento, nossa equipe vai até o local combinado e faz a retirada dos lixos eletrônicos. Funcionários uniformizados.
                            <br>
                            No momento da retirada dos equipamentos, serão fornecidos um recibo nominal e um relatório de descarte consciente. Contamos com empresas certificadas pelo Ibama e pela Cetesb para o processo de reciclagem e reutilização.
                        </blockquote>

                    </div>

                </div>
            </div>

        </div>
        <div class="container">

        </div>
    </section>
    <!--================About Area End =================-->


    <!--================ Start Portfolio Area =================
    <section class="pricing_area area-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="area-heading">
                        <h4>Conheça Nossos Planos</h4>
                        <p>Temos alguns modelos que sua empresa pode necessitar. Escolha o melhor.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single-pricing">
                        <div class="pricing-icon">
                            <i class="ti-home"></i>
                        </div>
                        <div class="single-pricing-content">
                            <h5>Standard</h5>
                            <h4>$25<span class="currency_line">/</span><span>month</span></h4>
                            <ul>
                                <li>2GB Bandwidth</li>
                                <li>Two Account</li>
                                <li>15GB Storage</li>
                                <li>Sale After Service</li>
                                <li>3 Host Domain</li>
                                <li>24/7 Support</li>
                            </ul>
                            <a href="#">Purchase Now</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="single-pricing">
                        <div class="pricing-icon">
                            <i class="ti-bag"></i>
                        </div>
                        <div class="single-pricing-content">
                            <h5>Business</h5>
                            <h4>$35<span class="currency_line">/</span><span>month</span></h4>
                            <ul>
                                <li>2GB Bandwidth</li>
                                <li>Two Account</li>
                                <li>15GB Storage</li>
                                <li>Sale After Service</li>
                                <li>3 Host Domain</li>
                                <li>24/7 Support</li>
                            </ul>
                            <a href="#">Purchase Now</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="single-pricing">
                        <div class="pricing-icon">
                            <i class="ti-car"></i>
                        </div>
                        <div class="single-pricing-content">
                            <h5>Premium</h5>
                            <h4>$45<span class="currency_line">/</span><span>month</span></h4>
                            <ul>
                                <li>2GB Bandwidth</li>
                                <li>Two Account</li>
                                <li>15GB Storage</li>
                                <li>Sale After Service</li>
                                <li>3 Host Domain</li>
                                <li>24/7 Support</li>
                            </ul>
                            <a href="#">Purchase Now</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="single-pricing">
                        <div class="pricing-icon">
                            <i class="ti-gift"></i>
                        </div>
                        <div class="single-pricing-content">
                            <h5>Ultimate</h5>
                            <h4>$55<span class="currency_line">/</span><span>month</span></h4>
                            <ul>
                                <li>2GB Bandwidth</li>
                                <li>Two Account</li>
                                <li>15GB Storage</li>
                                <li>Sale After Service</li>
                                <li>3 Host Domain</li>
                                <li>24/7 Support</li>
                            </ul>
                            <a href="#">Purchase Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    ================ End Pricing Area =================-->


    <!--================ Start Blog Area
    <section class="latest-blog-area area-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="area-heading">
                        <h4>Read Our Latest News</h4>
                        <p>Life firmament under them evening make after called dont saying likeness<br> isn't wherein also forth she'd air two without</p>
                    </div>           
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 col-md-6 ">
                    <div class="single-blog full_image">
                        <div class="thumb">
                            <img class="img-fluid w-100" src="img/blog/1.png" alt="">
                        </div>
                        <div class="single-blog-content">
                            <p class="tag">Software / Business</p>
                            <p class="date">March 10, 2019</p>
                            <h4>
                                <a href="#">Appear called is blessed good void had given from <br>
                                which Lights Saying image.</a>
                            </h4>
                            <div class="meta-bottom d-flex">
                                <p class="likes"><i class="ti-comments"></i> 02 Comments</p>
                                <p><i class="ti-heart"></i> 15 </p>
                            </div>

                        </div>

                    </div>

                </div>


                <div class="col-lg-4 col-md-6 ">
                    <div class="single-blog">
                        <div class="thumb">
                            <img class="img-fluid w-100" src="img/blog/2.png" alt="">
                        </div>
                        <div class="single-blog-content">
                            <p class="tag">Software / Business</p>
                            <p class="date">March 10, 2019</p>
                            <h4>
                                <a href="#">You living thing whose After
                                our third you also Us.</a>
                            </h4>
                            <div class="meta-bottom d-flex">
                                <p class="likes"><i class="ti-comments"></i> 02 Comments</p>
                                <p><i class="ti-heart"></i> 15 </p>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="col-lg-4 col-md-6 ">
                    <div class="single-blog">
                        <div class="thumb">
                            <img class="img-fluid w-100" src="img/blog/1.png" alt="">
                        </div>
                        <div class="single-blog-content">
                            <p class="tag">Software / Business</p>
                            <p class="date">March 10, 2019</p>
                            <h4>
                                <a href="#">You living thing whose After
                                our third you also Us.</a>
                            </h4>
                            <div class="meta-bottom d-flex">
                                <p class="likes"><i class="ti-comments"></i> 02 Comments</p>
                                <p><i class="ti-heart"></i> 15 </p>
                            </div>

                        </div>
                    </div>

                </div>


                <div class="col-lg-8 col-md-6">
                    <div class="single-blog full_image">
                        <div class="thumb">
                            <img class="img-fluid w-100" src="img/blog/4.png" alt="">
                        </div>
                        <div class="single-blog-content">
                            <p class="tag">Software / Business</p>
                            <p class="date">March 10, 2019</p>
                            <h4>
                                <a href="#">You living thing whose After
                                our third you also Us.</a>
                            </h4>
                            <div class="meta-bottom d-flex">
                                <p class="likes"><i class="ti-comments"></i> 02 Comments</p>
                                <p><i class="ti-heart"></i>15</p>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    ================ End Blog Area =================-->

<?php include 'footer.html'; ?>